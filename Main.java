public class Main
{
    public static void main(String[] args)
    {
        Request request = new Request(System.getProperty("type").equals("A") ? "handlerA" : "handlerB");
        Client client = new Client(request);
    }
}
