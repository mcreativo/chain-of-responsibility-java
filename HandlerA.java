public class HandlerA implements Handler
{
    @Override
    public void handleRequest(Request request)
    {
        System.out.println("HandlerA doing something with request");
    }
}
