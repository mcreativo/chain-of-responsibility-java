public class Client
{
    public Client(Request request)
    {
        Handler handlerA = new HandlerA();
        Handler handlerB = new HandlerB();

        if (request.getAttribute().equals("handlerA"))
        {
            handlerA.handleRequest(request);
        }
        else
        {
            handlerB.handleRequest(request);
        }
    }
}
