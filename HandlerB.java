public class HandlerB implements Handler
{
    @Override
    public void handleRequest(Request request)
    {
        System.out.println("HandlerB doing something with request");
    }
}
