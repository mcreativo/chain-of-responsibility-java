public class Request
{
    private String attribute;

    public Request(String attribute)
    {
        this.attribute = attribute;
    }

    public String getAttribute()
    {
        return attribute;
    }
}
